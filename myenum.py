from enum import Enum
# class enum Hướng di chuyển
# tính theo độ
class Direction(Enum):
    UP    = 0
    LEFT  = 90
    DOWN  = 180
    RIGHT = 270
# class enum Loại cho cell trong grid
class Item(Enum):
    EMPTY = -1
    WALL  = 0
    SNAKE = 1
    APPLE = 2