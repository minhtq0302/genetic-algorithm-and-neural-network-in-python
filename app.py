import configparser
from typing import Tuple
import pygame
from cell import Cell
from grid import Grid
from myenum import Direction, Item
from snake import Snake
from window import WindowGame
import numpy as np
import random as rd

# Class Application dùng để khởi tạo Giao diện hiển thị trò chơi
class Application:
    def __init__(self, config):
        # Cấu hình giao diện hiển thị
        self.board_size = eval(config.get('Game', 'board_size'))
        self.seed = eval(config.get('Game', 'seed'))
        self.grid = Grid(self.board_size)
        
        # tạo các biến cho app
        np.random.seed(self.seed)
        rd.seed(self.seed)
        self.snakes = []
        self.apples = []

        # Hiển thị trò chơi
        self.show = eval(config.get('WindowGame', 'render'))
        if self.show:
            # Khởi tạo màng hình pygame
            self.cell_size = eval(config.get('WindowGame', 'cell_size'))
            screen_size = (self.cell_size * (self.board_size[1] ),
                           self.cell_size * (self.board_size[0]))
            self.pygame_window = pygame.display.set_mode(screen_size)
            # Cập nhật FPS tốc độ trò chơi
            self.clock = pygame.time.Clock() 
            self.fps_play = eval(config.get('WindowGame', 'fps_play'))

            self.window_game = WindowGame(self, self.pygame_window, 
                                          cell_size = self.cell_size)
            self.show_grid = eval(config.get('WindowGame', 'show_grid'))
            
        
        # Trạng thái trong trò chơi
        # ------
        self._pause = True
        self._run = False
        
        # Tham số cho đối tượng snake
        # Sau này cấu hình thêm ở đây
        # -------------------
        self.snake_params = {
            "length": eval(config.get('Snake', 'length'))
            }
    
    def add_apple(self):
        apple = self.generate_apple()
        self.apples.append(apple)
        # Cập nhât grid
        self.grid.set_cell(apple)
      
        
    def generate_apple(self):
        height, width = self.grid.shape 
        available_coord = []
        # Kiểm tra các cell trống trong grid
        for i in range(height):
            for j in range(width):
                cell = self.grid[i, j]
                # Nếu cell trống thì thêm vào danh sách khả dụng
                if cell.is_empty():
                    available_coord.append(cell.coord)
                    
        # Chọn ngẫu nhiên trong danh sách khả dụng
        # Lấy tọa độ của cell
        coord = rd.choices(available_coord)[0]
        apple = Cell(coord, Item.APPLE)     
        return apple
    
    def add_snake(self, snake=None, **kwargs):
        if snake is None:
            snake = Snake(self, **kwargs)
        else:
            # Cập nhật snake trong game
            snake.app = self

        # Thêm đối tượng snake vào danh sách snakes
        self.snakes.append(snake)
        # cập nhật lại grid
        self.grid.set_cell(*snake.body)
    
    def clean(self):
        for snake in self.snakes:
            snake.kill()
        self.snakes = []     
        for apple in self.apples:
            self.grid.set_empty(apple.coord)
        self.apples = []

    def start(self, snake=None, **kwargs):
        self.clean()
        self.add_snake(snake, **kwargs)
        self.add_apple()  

    def _player_controler(self, snake):
        # Thoát trò chơi
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._run = False
                pygame.quit()
                quit()
        
        # Cập nhật hướng di chuyển bằng bàn phím 
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            snake.direction = Direction.UP
            self._pause = False
        elif keys[pygame.K_RIGHT]:
            snake.direction = Direction.RIGHT
            self._pause = False
        elif keys[pygame.K_DOWN]:
            snake.direction = Direction.DOWN
            self._pause = False
        elif keys[pygame.K_LEFT]:
            snake.direction = Direction.LEFT
            self._pause = False
        
        # Tạm dừng trò chơi
        elif keys[pygame.K_SPACE]:
            self._pause = not self._pause
            #p#rint(snake.compute_input())
            
        # Khởi động lại trò chơi
        elif keys[pygame.K_r]:
            self._pause = True
            self.seed = snake.seed
            snake = Snake(self, **snake.get_params())
            self.start(snake)
        
        # Hiển thị lưới trên bản đồ trò chơi (Grid)
        elif keys[pygame.K_g]:
            self.show_grid = not self.show_grid
                
        # Tăng/Giảm tốc độ FPS
        elif keys[pygame.K_KP_PLUS]:
            self.fps_play += 1 
        elif keys[pygame.K_KP_MINUS]:
            self.fps_play -= 1
            
        return snake
                       

    def play(self, snake=None):      
        if snake is None:
            snake = Snake(self, **self.snake_params) 
        else:
            snake.app = self

        self.start(snake)

        # Vòng lặp vô tậng cho tới khi snake chết
        self._run = True
        while self._run:
            # Hiển thị trò chơi
            if self.show:
                self.window_game.draw(show_grid=self.show_grid)
                self.clock.tick(self.fps_play)
                # Người chơi điều kiển
                snake = self._player_controler(snake)

            # Con rắn luôn luôn di chuyển nếu 
            # Trạng thái của con rắn không phải là pause
            if not self._pause:
                # Cho con rắn di chuyển
                is_alive = snake.move()
                # Nếu con rắn chết thì dừng trò chơi và log ra điểm số
                if not is_alive:
                    print("----------------")
                    print("Bạn đã chết!")
                    print("Điểm số    : {}".format(snake.score))
                   
                    # Khởi động lại trò chơi
                    # với cùng config / seed / params
                    self.seed = snake.seed
                    snake = Snake(self, **snake.get_params())
                    self.start(snake)
                    # Tạm dừng trò chơi trước khi 
                    # người chơi sẵn sàng bằng phím di chuyển bất kỳ (left, right, top, down)
                    self._pause = True

if __name__ == "__main__":
    # Nạp file cấu hình config.ini
    config_file = "./config.ini"
    config = configparser.ConfigParser()
    config.read(config_file)
    snake_game = Application(config)
    snake_game.play()