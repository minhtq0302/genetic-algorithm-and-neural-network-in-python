from typing import Tuple
from myenum import Item
from cell import Cell

# Class grid thể hiện bản đồ trò chơi
# Bao gồm cả SNAKE
class Grid:
    def __init__(self, shape, fill_value=Item.EMPTY):
        self.shape = shape
        self.maps = self._init_grid(fill_value)

    def _init_grid(self, fill_value):
        height, width = self.shape
        maps = []
        for i in range(height):
            row = []
            for j in range(width):
                coord = (i, j)
                cell = Cell(coord, fill_value)
                row.append(cell)
            maps.append(row)
        return maps
    
    def set_wall(self, *coords):
        for coord in coords:
            self[coord] = Cell(coord, Item.WALL)
    
    def set_empty(self, *coords):
        for coord in coords:
            self[coord] = Cell(coord, Item.EMPTY)
    
    def set_apple(self, *coords):
        for coord in coords:
            self[coord] = Cell(coord, Item.APPLE)
        
    def set_snake(self, *coords):
        for coord in coords:
            self[coord] = Cell(coord, Item.SNAKE)
        
    def set_cell(self, *cells):
        for cell in cells:
            self[cell.coord] = cell
    
    def is_wall(self, cell):
        grid_cell = self[cell.coord]
        return grid_cell.is_wall()
            
    def is_empty(self, cell):
        grid_cell = self[cell.coord]
        return grid_cell.is_empty()

    def is_apple(self, cell):
        grid_cell = self[cell.coord]
        return grid_cell.is_apple()

    def is_snake(self, cell):
        grid_cell = self[cell.coord]
        return grid_cell.is_snake()
    
    def is_outside(self, cell):
        # lấy ra kích thước của grid
        height, width = self.shape
        i, j = cell.coord
        # kiểm tra tọa độ
        if (i < 0) or (j < 0) or (i >= height) or (j >= width):
            return True
        return False
        
    # Lấy ra danh sách 1 row
    def row(self, index):
        return self.maps[index]
    
    # lấy ra danh sách 1 cột
    def col(self, index):
        return [row[index] for row in self.maps]
    
            
    def __getitem__(self, tuple_index):
        i, j = tuple_index
        return self.maps[i][j]
    
    
    def __setitem__(self, tuple_index, value):
        i, j = tuple_index
        self.maps[i][j] = value
    
    def __str__(self):
        height, width = self.shape
        grid = ""
        for i in range(height):
            row = "\n\t"
            for j in range(width):
                if self[i, j].is_wall():
                    row += "##"
                elif self[i, j].is_empty():
                    row += "  "
                elif self[i, j].is_apple():
                    row += " *"
                elif self[i, j].is_snake():
                    row += "[]"
            grid += row
        return grid