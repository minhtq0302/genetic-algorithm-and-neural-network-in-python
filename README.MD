## Giới thiệu
Snake là một trò chơi mà mục tiêu của bạn là thu thập táo
- Mỗi lần bạn thu thập một quả táo, bạn sẽ phát triển.
- Nếu bạn chạy vào một bức tường, bạn sẽ thua.
- Nếu bạn gặp phải chính mình, bạn sẽ thua

## Mã nguồn
Ngôn ngữ Python được viết trên Visual Studio Code 1.71.0-insider

Clone `git clone https://gitlab.com/minhtq0302/genetic-algorithm-and-neural-network-in-python.git`

## Cài đặt thư viện hỗ trợ

- pygame
- numpy
- Python3.6+

Để cài đặt các phụ thuộc, run `pip3 install -r requirements.txt`

## Chạy trò chơi
run `python app.py`