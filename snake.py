from typing import Optional, Tuple
import numpy as np
import random as rd
from cell import Cell
from myenum import Direction, Item

class Snake:
    def __init__(self, app, length:int = 3,  **kwargs ):
        self.app = app
        self.seed = app.seed
        self.direction = None
        self.score = 0
        np.random.seed(self.seed)
        rd.seed(self.seed)
        self.length = length
        self.body = self._init_body()

    def _init_body(self):
        body = []
        shape = self.app.grid.shape
        # Tọa độ của Đuôi Snake
        tail_i = rd.randint(1 + self.length , shape[0] - 2 - self.length)
        tail_j = rd.randint(1 + self.length , shape[1] - 2 - self.length)
        coord = (tail_i, tail_j)
        print("Tail coord ", coord)
        body_set = {coord}
        snake_cell = Cell(coord, Item.SNAKE)
        body.append(snake_cell)
        self.app.grid.set_cell(snake_cell)

        for _ in range(self.length - 1):
            print(f"-----Length:{_}-----")
            i, j = coord
            possible_coord = list({(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)} - body_set)
            print("possible_coord ", possible_coord)
            idx = np.random.choice(np.arange(len(possible_coord)))
            coord = possible_coord[idx]
            print("chose coord" , coord)
            snake_cell = Cell(coord, Item.SNAKE)
            body.append(snake_cell)
            body_set = body_set.union({coord})
            self.app.grid.set_cell(snake_cell) 
        print("-----") 
        print("body_set ", body_set)
        return body

    def move(self):
        # Lấy ra bản đồ grid hiện tại
        grid = self.app.grid
        # Hàm trả về giá trị đầu snake mới có kiểu Cell
        new_head = self._next_move()
        # Kiểm tra hướng di chuyển có nằm bên ngoài grid
        if grid.is_outside(new_head):
            # trả về false vì snake đã chết
            return False
        elif grid.is_wall(new_head):
            # trả về false vì snake đã chết
            return False
        # Kiểm tra dụng thân SNAKE
        elif grid.is_snake(new_head):
            # trả về false vì snake đã chết
            return False
        
        # Kiểm tra nếu dụng APPLE
        elif grid.is_apple(new_head):
            # Cập nhật body snake thêm 1 cell
            self.body.append(new_head)
   
            # Cập nhật đầu snake mới vào grid
            grid.set_cell(new_head)   
            # tạo ngẫu nhiên APPLE mới
            self.app.add_apple()

            # Cập nhật điểm số
            # Mỗi APPLE ăn được 1 điểm
            self.score += 1
           
        # Di chuyển body Snake
        else:
            # Cập nhật body snake
            self.body.append(new_head)
            # Lấy ra đuôi của snake ra khỏi body snake
            previous_tail = self.body.pop(0)
            
            # Cập nhật đầu snake vào grid
            grid.set_cell(new_head)
            # Cập nhật đuôi snake thành trống 
            grid.set_empty(previous_tail.coord)
        
        # Trả về true vì snake còn sống
        return True

    def _next_move(self):
        # Lấy ra Cell của đầu Snake hiện tại
        head = self.body[-1] # Phần tử cuối cùng của mảng, mỗi phần tử của mảng là 1 Cell
        # Lấy ra coord của Cell là 1 Tuple[int,int]
        head_coord = head.coord
        
        # Chọn hướng di chuyển để lấy tọa độ mới của đầu Snake
        if self.direction is Direction.UP:
            new_head_coord = (int(head_coord[0] - 1), int(head_coord[1]))
        elif self.direction is Direction.LEFT:
            new_head_coord = (int(head_coord[0]), int(head_coord[1] - 1))
        elif self.direction is Direction.DOWN:
            new_head_coord = (int(head_coord[0] + 1), int(head_coord[1]))     
        elif self.direction is Direction.RIGHT:
            new_head_coord = (int(head_coord[0]), int(head_coord[1] + 1))       
        # Tạo 1 Cell đầu Snake với coord Tuple[int,int]
        new_head = Cell(new_head_coord, Item.SNAKE)   # type: ignore
        # return ra Cell đầu Snake mới
        return new_head

    # Trả về các tham số của Snake
    def get_params(self):
        params = {"length": self.length}
        return params

    # Khi Snake chết
    # Cập nhật lại grid
    # Cập nhật tất cả các Cell trong grid về EMPTY qua mảng body Snake
    def kill(self):
        for cell in self.body:
            self.app.grid.set_empty(cell.coord)