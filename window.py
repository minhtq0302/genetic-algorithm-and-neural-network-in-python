import pygame
from pygame import gfxdraw
import numpy as np
from myenum import Item

# class vẽ giao diện pygame
class WindowGame:
    def __init__(self, app, pygame_window,
                 cell_size = 50 
                ):
        self.pygame_window = pygame_window
        self.app = app
        self.cell_size = cell_size
        self.color_template = {"background":     (255, 255, 255),
                              "empty":          (37, 54, 69),
                              "wall":           (32, 44, 55),
                              "snake":          (46, 142, 212),
                              "snake_head":     (46, 142, 255),
                              "apple":          (227, 68, 52),
                             }

    def draw_game(self, show_grid):
        # truyền các tham số
        game = self.app
        grid = game.grid
        height, width = grid.shape
        
        # Vẽ bản đồ grid
        for i in range(height):
            for j in range(width):
                # Vẽ tường WALL
                if grid[i, j].is_wall():
                    color = self.color_template['wall']
                    pygame.draw.rect(self.pygame_window, color, 
                                     (j * self.cell_size,i * self.cell_size, 
                                      self.cell_size, self.cell_size))  
               
                # Vẽ bản đồ dạng grid
                elif grid[i, j].is_empty():
                    color = self.color_template['empty']
                    if show_grid and ((i +j)%2 == 1):
                        color = self.color_template['wall']
                    pygame.draw.rect(self.pygame_window, color, 
                                     (j * self.cell_size,i * self.cell_size, 
                                      self.cell_size, self.cell_size))  
                
                # Vẽ Body Snake
                elif grid[i, j].is_snake():
                    color = self.color_template['snake']
                    pygame.draw.rect(self.pygame_window, color, 
                                     (j * self.cell_size,i * self.cell_size, 
                                      self.cell_size, self.cell_size))  
                
                # Vẽ APPLE
                elif grid[i, j].is_apple():
                    color = self.color_template['apple']
                    pygame.draw.rect(self.pygame_window, color, 
                                     (j * self.cell_size,i * self.cell_size, 
                                      self.cell_size, self.cell_size))  
       
        # Vẽ Đầu Snake
        for snake in self.app.snakes:
            i, j = snake.body[-1].coord
            color = self.color_template['snake_head']
            pygame.draw.rect(self.pygame_window, color, 
                            (j * self.cell_size,i * self.cell_size, 
                             self.cell_size, self.cell_size))   
            
    
   
        
    def draw(self, show_grid=True):
        # Vẽ tô màu nền background
        height, width = pygame.display.get_surface().get_size()
        color = self.color_template["background"]
        pygame.draw.rect(self.pygame_window, color, 
                                     (0, 0, width, height))  
        
        # Vẽ các đối tượng trong grid
        self.draw_game(show_grid)

        # Cập nhật màng hình windown pygame
        pygame.display.update()


