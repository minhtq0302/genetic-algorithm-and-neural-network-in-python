from typing import Tuple
from myenum import Item

#class Cell dùng để định nghĩa 1 cell 
#Trong cell có tọa độ Tuple[int,int]
#Cell thể hiện loại enum APPLE, SNAKE, WALL, EMPTY 
class Cell:
    def __init__(self, coord : Tuple[int,int], item : Item):
        # Tạo tọa độ kiểu Tuple[int,int]
        self.coord = coord
        # Item là kiểu enum APPLE, SNAKE, WALL, EMPTY 
        self.item = item
        # Tên của kiểu myenum
        self.name = item.name
        # Giá trị của kiểu myenum
        self.value = item.value
    
    # Kiểm tra cell này là TƯỜNG
    # Return bool
    def is_wall(self):
        return self.item is Item.WALL
    # Kiểm tra cell này là RỔNG
    # Return bool
    def is_empty(self):
        return self.item is Item.EMPTY
    # Kiểm tra cell này là SNAKE
    # Return bool
    def is_snake(self):
        return self.item is Item.SNAKE
    # Kiểm tra cell này là APPLE
    # Return bool
    def is_apple(self):
        return self.item is Item.APPLE
    
    